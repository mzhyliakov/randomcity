package com.example.randomcity

import com.example.randomcity.app.Utils
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.entities.CityColor
import com.example.randomcity.domain.entities.CityLocation
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    lateinit var cities: List<City>
    val wroclaw = City("Wrocław", CityLocation(.0, .0), CityColor("", 0))
    val krakow = City("Kraków", CityLocation(.0, .0), CityColor("", 0))
    val gdansk = City("Gdańsk", CityLocation(.0, .0), CityColor("", 0))

    @Before
    fun initValues() {
        cities = listOf(
            City("Gdańsk", CityLocation(.0, .0), CityColor("", 0)),
            City("Warszawa", CityLocation(.0, .0), CityColor("", 0)),
            City("Poznań", CityLocation(.0, .0), CityColor("", 0)),
            City("Białystok", CityLocation(.0, .0), CityColor("", 0))
        ).sortedBy { item -> item.name }
    }

    @Test
    fun searchForWroclaw_isOk() {
        val pos = Utils.getItemPos(cities, wroclaw)
        assertEquals(4, pos)
    }

    @Test
    fun searchForKrakow_isOk() {
        val pos = Utils.getItemPos(cities, krakow)
        assertEquals(2, pos)
    }

    @Test
    fun searchForGdansk_isOk() {
        val pos = Utils.getItemPos(cities, gdansk)
        assertEquals(1, pos)
    }
}