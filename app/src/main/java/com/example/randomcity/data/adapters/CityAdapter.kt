package com.example.randomcity.data.adapters

import com.example.randomcity.data.db.CityEntity
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.entities.CityColor
import com.example.randomcity.domain.entities.CityLocation

object CityAdapter {

    fun toDBEntity(city: City): CityEntity {
        return with(city) {
            CityEntity(
                name = name,
                lat = location.lat.toFloat(),
                lon = location.lon.toFloat(),
                colorName = color.name,
                colorValue = color.value,
                createdAt = createdAt
            )
        }
    }

    fun fromDBEntity(entity: CityEntity): City {
        return with(entity) {
            City(
                name = name,
                CityLocation(lat.toDouble(), lon.toDouble()),
                CityColor(colorName, colorValue),
                createdAt = createdAt
            )
        }
    }
}