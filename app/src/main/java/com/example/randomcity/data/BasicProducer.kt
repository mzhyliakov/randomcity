package com.example.randomcity.data

import com.example.randomcity.domain.entities.City
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

abstract class BasicProducer(intervalInMillis: Long) {
    private var disposable: Disposable? = null

    private val timerObservable = Observable.interval(
        intervalInMillis,
        intervalInMillis, TimeUnit.MILLISECONDS
    )

    private val isStarted = AtomicBoolean(false)

    fun observe(
        onSuccess: ((t: City?) -> Unit),
        onError: ((t: Throwable) -> Unit),
        onFinished: () -> Unit = {}
    ) {
        synchronized(this) {
            if (!isStarted.get()) {
                disposable = timerObservable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterTerminate(onFinished)
                    .subscribe({
                        onSuccess(createNextCity())
                    }, onError)
                isStarted.set(true)
            }
        }
    }

    fun dispose() {
        synchronized(this) {
            isStarted.set(false)
            disposable?.dispose()
        }
    }

    abstract fun createNextCity(): City?
}