package com.example.randomcity.data

import com.example.randomcity.domain.entities.City
import java.util.concurrent.TimeUnit

class CityProducer(intervalInMillis: Long = TimeUnit.SECONDS.toMillis(5)) :
    BasicProducer(intervalInMillis) {

    private val cityGenerator by lazy { CityGenerator() }

    override fun createNextCity(): City? = cityGenerator.generate()
}