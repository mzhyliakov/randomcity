package com.example.randomcity.data

import android.util.Log
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.entities.CityColor
import kotlin.random.Random

class CityGenerator {

    private val cityNames = mutableListOf<String>()
    private val colorNames = mutableListOf<String>()
    private val random = Random.Default

    fun generate(): City? {
        verifyAndPrepareSourceData()

        val cityName = cityNames[random.nextInt(cityNames.size)]
        val colorName = colorNames[random.nextInt(colorNames.size)]

        DataCollection.cityWithLocations[cityName]?.let { location ->
            DataCollection.colorValues[colorName]?.let {colorValue ->
                removeEntitiesFromSourceData(cityName, colorName)

                val color = CityColor(colorName, colorValue)
                return City(cityName, location,  color)
            }
        }
        return null
    }

    private fun verifyAndPrepareSourceData() {
        if(colorNames.isEmpty()) {
            Log.d("TAG", "its time to refresh colors!")
            colorNames.addAll(DataCollection.colors)
        }

        if(cityNames.isEmpty()) {
            Log.d("TAG", "its time to refresh cities!")
            cityNames.addAll(DataCollection.cities)
        }
    }

    private fun removeEntitiesFromSourceData(cityName: String, colorName: String) {
        cityNames.remove(cityName)
        colorNames.remove(colorName)
        Log.d("TAG", "there are ${cityNames.size} cities and ${colorNames.size} colors left")
    }
}