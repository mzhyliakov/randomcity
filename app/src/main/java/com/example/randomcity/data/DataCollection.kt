package com.example.randomcity.data

import android.graphics.Color
import com.example.randomcity.domain.entities.CityLocation

object DataCollection {
    //based on this data:
    val cities = listOf("Gdańsk", "Warszawa", "Poznań", "Białystok", "Wrocław", "Katowice", "Kraków")
    val colors = listOf("Yellow", "Green", "Blue", "Red", "Black", "White")

    //we have more adapted once:
    val cityWithLocations = mapOf(
        "Gdańsk" to CityLocation(54.372158, 18.638306),
        "Warszawa" to CityLocation(52.229676, 21.012229),
        "Poznań" to CityLocation(52.406376, 16.925167),
        "Białystok" to CityLocation(53.1324886, 23.1688403),
        "Wrocław" to CityLocation(51.107883, 	17.038538),
        "Katowice" to CityLocation(50.264892, 19.023782),
        "Kraków" to CityLocation(	50.049683, 19.944544)
    )

    val colorValues = mapOf(
        "Yellow" to Color.parseColor("#FFB701"),
        "Green" to Color.parseColor("#8AAE50"),
        "Blue" to Color.parseColor("#6EC6CA"),
        "Red" to Color.parseColor("#F05C31"),
        "Black" to Color.BLACK,
        "White" to Color.WHITE
    )
}