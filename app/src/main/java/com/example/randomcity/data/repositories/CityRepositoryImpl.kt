package com.example.randomcity.data.repositories

import com.example.randomcity.data.adapters.CityAdapter
import com.example.randomcity.data.db.AppDatabase
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.repositories.CityRepository
import io.reactivex.Completable
import io.reactivex.Single

class CityRepositoryImpl(private val database: AppDatabase) : CityRepository {
    override fun loadAll(): Single<List<City>> {
        return database.cityDao().getAll()
            .map { records ->
                records.map { record -> CityAdapter.fromDBEntity(record) }
            }
    }

    override fun save(city: City): Completable {
        return database.cityDao().insert(CityAdapter.toDBEntity(city))
    }
}