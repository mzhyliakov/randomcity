package com.example.randomcity.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface CityDao {
    @Query("select * from cities")
    fun getAll(): Single<List<CityEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: CityEntity): Completable
}