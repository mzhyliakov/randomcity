package com.example.randomcity.data.db

import android.content.Context
import androidx.room.*

@Database(entities = [CityEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cityDao(): CityDao


    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private const val name = "cities_db"

        fun getInstance(context: Context): AppDatabase = instance ?: synchronized(this) {
            instance ?: buildDatabase(context, name).also {
                instance = it
            }
        }

        @Suppress("SameParameterValue")
        private fun buildDatabase(appContext: Context, name: String): AppDatabase {
            return Room.databaseBuilder(appContext, AppDatabase::class.java, name)
                .fallbackToDestructiveMigration().build()
        }
    }
}