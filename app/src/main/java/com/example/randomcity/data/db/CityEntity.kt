package com.example.randomcity.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cities")
data class CityEntity(
    @PrimaryKey val uid: Int? = null,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "lat") val lat: Float,
    @ColumnInfo(name = "lon") val lon: Float,
    @ColumnInfo(name = "colorName") val colorName: String,
    @ColumnInfo(name = "colorValue") val colorValue: Int,
    @ColumnInfo(name = "createdAt") val createdAt: Long
)