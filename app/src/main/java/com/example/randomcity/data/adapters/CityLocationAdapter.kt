package com.example.randomcity.data.adapters

import com.example.randomcity.domain.entities.CityLocation
import com.google.android.gms.maps.model.LatLng

object CityLocationAdapter {

    fun toLatLng(cityLocation: CityLocation): LatLng {
        return LatLng(cityLocation.lat, cityLocation.lon)
    }
}