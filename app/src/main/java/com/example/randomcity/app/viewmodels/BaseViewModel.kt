package com.example.randomcity.app.viewmodels

import androidx.lifecycle.ViewModel
import com.example.randomcity.domain.usecases.UseCase

open class BaseViewModel(vararg useCases: UseCase): ViewModel() {

    private var useCaseList: MutableList<UseCase> = mutableListOf()

    init {
        useCaseList.addAll(useCases)
    }

    override fun onCleared() {
        super.onCleared()
        useCaseList.forEach { useCase -> useCase.dispose() }
    }
}