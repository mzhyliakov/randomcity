package com.example.randomcity.app.extensions

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("tint_color")
fun ImageView.setTintColor(color: Int) {
    setColorFilter(color)
}