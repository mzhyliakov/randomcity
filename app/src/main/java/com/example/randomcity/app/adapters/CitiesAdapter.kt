package com.example.randomcity.app.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcity.app.Utils
import com.example.randomcity.databinding.ItemCityBinding
import com.example.randomcity.domain.entities.City

class CityItemVH(val binding: ItemCityBinding) : RecyclerView.ViewHolder(binding.root)

class CitiesAdapter(private val listener: OnCityItemClickListener? = null) :
    RecyclerView.Adapter<CityItemVH>() {

    private val cities = mutableListOf<City>()

    fun setCities(cities: List<City>) {
        this.cities.clear()
        this.cities.addAll(cities)
        notifyDataSetChanged()
    }

    fun addCity(city: City): Int {
        if (!isSuchRecordAdded(city)) {
            val pos = Utils.getItemPos(cities, city)
            cities.add(pos, city)
            notifyItemInserted(pos)
            return pos
        }
        return -1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityItemVH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemCityBinding = ItemCityBinding.inflate(layoutInflater, parent, false)
        return CityItemVH(binding)

    }

    override fun onBindViewHolder(holder: CityItemVH, position: Int) {
        val cityItem = cities[position]

        holder.binding.cityName = cityItem.name
        holder.binding.cityColor = cityItem.color.value
        holder.binding.indicatorColor = Utils.getContrastColor(cityItem.color.value)
        holder.binding.dateTime = Utils.stringifyDateTime(cityItem.createdAt)

        holder.binding.root.setOnClickListener {
            listener?.onCityItemClicked(cityItem)
        }
    }

    override fun getItemCount() = cities.size

    private fun isSuchRecordAdded(city: City): Boolean {
        return cities.find { item -> item.createdAt == city.createdAt } != null
    }
}