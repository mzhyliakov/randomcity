package com.example.randomcity.app

import android.graphics.Color
import com.example.randomcity.domain.entities.City
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator

object Utils {
    private val dateTimeFormatter = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ROOT)

    fun stringifyDateTime(millis: Long): String = dateTimeFormatter.format(Date(millis))

    fun getItemPos(list: List<City>, city: City): Int {
        val cityNameComparator =
            Comparator<City> { o1, o2 ->
                when {
                    o1 == null -> -1
                    o2 == null -> 1
                    else -> o1.name.compareTo(o2.name)
                }
            }

        //according to documentation binarySearch returns found position as: (-insertionPoint - 1)
        //so we have to make some transformations before supply this value as position of insertion
        val binarySearchResult = list.binarySearch(city, cityNameComparator)
        return when {
            binarySearchResult < 0 -> (binarySearchResult + 1).unaryMinus()
            else -> binarySearchResult
        }
    }

    fun getContrastColor(originalColor: Int): Int {
        return when (originalColor) {
            Color.WHITE -> {
                Color.BLACK
            }
            else -> Color.WHITE
        }
    }
}