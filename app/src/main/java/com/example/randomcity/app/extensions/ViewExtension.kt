package com.example.randomcity.app.extensions

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visible")
fun View.setIsVisibility(visible: Boolean) {
    val visibility = if (visible) View.VISIBLE else View.GONE
    setVisibility(visibility)
}