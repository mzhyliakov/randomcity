package com.example.randomcity.app.adapters

import com.example.randomcity.domain.entities.City

interface OnCityItemClickListener {
    fun onCityItemClicked(city: City)
}