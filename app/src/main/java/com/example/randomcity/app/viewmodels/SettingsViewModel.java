package com.example.randomcity.app.viewmodels;

import androidx.lifecycle.MutableLiveData;

public class SettingsViewModel extends BaseViewModel {

    public MutableLiveData<Boolean> isAdditionalInfoEnabled = new MutableLiveData<>();
}
