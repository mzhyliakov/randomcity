package com.example.randomcity.app.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.randomcity.app.Utils
import com.example.randomcity.data.CityProducer
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.usecases.LoadCitiesUseCase
import com.example.randomcity.domain.usecases.SaveCityUseCase

class MainViewModel(
    private val loadCitiesUseCase: LoadCitiesUseCase,
    private val saveCityUseCase: SaveCityUseCase,
) : BaseViewModel() {

    private val producer = CityProducer()
    val currentCity = MutableLiveData<City>()
    val cities = MutableLiveData<MutableList<City>>(mutableListOf())

    fun startCityGenerator() {
        producer.observe(
            onSuccess = { city ->
                city?.let {
                    addCityToCache(city)
                    saveCity(city)
                }
                currentCity.value = city
            },
            onError = { t ->
                Log.w("TAG", "error happened: ${t.message}")
                currentCity.value = null
            },
            onFinished = {}
        )
    }

    fun stopCityGenerator() {
        producer.dispose()
    }

    override fun onCleared() {
        producer.dispose()
        super.onCleared()
    }

    fun loadCities() {
        loadCitiesUseCase.execute(
            onSuccess = { cities ->
                this.cities.value = cities.sortedBy { city -> city.name }.toMutableList()
                Log.d("TAG", "cities loaded!")
            },
            onError = { t ->
                cities.value = null
                Log.w("TAG", "cities not loaded, error: ${t.message}")
            },
            onFinished = {},
            null
        )
    }

    fun saveCity(city: City) {
        saveCityUseCase.execute(
            onComplete = {
                Log.d("TAG", "city saved!")
            },
            onError = { t ->
                Log.d("TAG", "city not saved: error: ${t.message}")
            },
            onFinished = {},
            params = city
        )
    }

    private fun addCityToCache(city: City) {
        cities.value?.apply {
            val pos = Utils.getItemPos(this, city)
            add(pos, city)
        }
    }
}