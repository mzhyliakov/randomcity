package com.example.randomcity.app.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.example.randomcity.R;
import com.example.randomcity.app.viewmodels.SettingsViewModel;
import com.example.randomcity.databinding.FragmentAboutBinding;

import org.jetbrains.annotations.NotNull;

import static com.example.randomcity.app.Constants.SETTINGS_FILE;

public class AboutFragment extends BaseFragment {

    private SharedPreferences sharedPreferences;
    private SettingsViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(this).get(SettingsViewModel.class);

        FragmentAboutBinding binding = FragmentAboutBinding.inflate(inflater);
        binding.tvAbout.setText(Html.fromHtml(getString(R.string.about_description)));
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = view.getContext().getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE);
        viewModel.isAdditionalInfoEnabled.setValue(isAdditionalInfoOptionEnabled());

        viewModel.isAdditionalInfoEnabled.observe(getViewLifecycleOwner(), enabled1 -> {
            sharedPreferences
                    .edit()
                    .putBoolean(getString(R.string.additional_info_key), enabled1)
                    .apply();
        });


    }

    @Override
    public boolean isToolbarShown() {
        return true;
    }

    @NotNull
    @Override
    public String getTitle() {
        return getString(R.string.about);
    }
}