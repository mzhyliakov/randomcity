package com.example.randomcity.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.randomcity.R
import com.example.randomcity.app.extensions.viewModelFactory
import com.example.randomcity.app.viewmodels.MainViewModel
import com.example.randomcity.data.db.AppDatabase
import com.example.randomcity.data.repositories.CityRepositoryImpl
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.usecases.LoadCitiesUseCase
import com.example.randomcity.domain.usecases.SaveCityUseCase

class SplashScreenFragment : BaseFragment() {

    private val viewModel: MainViewModel by activityViewModels(factoryProducer = viewModelFactory {
        val repo = CityRepositoryImpl(AppDatabase.getInstance(requireContext().applicationContext))
        val loadCitiesUseCase = LoadCitiesUseCase(repo)
        val saveCityUseCase = SaveCityUseCase(repo)
        MainViewModel(loadCitiesUseCase, saveCityUseCase)
    })

    private val newCityObserver = Observer<City> {
        switchTo(R.id.cityListFragment)
    }

    private val loadedCacheObserver = Observer<List<City>> {
        Log.d("TAG", "splash screen, loaded: ${it.size} cities")
        viewModel.startCityGenerator()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onResume() {
        super.onResume()
        viewModel.currentCity.observe(viewLifecycleOwner, newCityObserver)
        viewModel.cities.observe(viewLifecycleOwner, loadedCacheObserver)
        viewModel.loadCities()
    }

    override fun onPause() {
        super.onPause()
        viewModel.currentCity.removeObserver(newCityObserver)
        viewModel.cities.removeObserver(loadedCacheObserver)
    }

    override fun isSysBarShown() = false
}