package com.example.randomcity.app.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcity.R
import com.example.randomcity.app.adapters.CitiesAdapter
import com.example.randomcity.app.adapters.OnCityItemClickListener
import com.example.randomcity.app.extensions.viewModelFactory
import com.example.randomcity.app.viewmodels.MainViewModel
import com.example.randomcity.data.adapters.CityLocationAdapter
import com.example.randomcity.data.db.AppDatabase
import com.example.randomcity.data.repositories.CityRepositoryImpl
import com.example.randomcity.databinding.FragmentMasterDetailsBinding
import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.usecases.LoadCitiesUseCase
import com.example.randomcity.domain.usecases.SaveCityUseCase
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions

class MasterDetailsFragment : BaseFragment(), OnCityItemClickListener, OnMapReadyCallback {

    private val viewModel: MainViewModel by activityViewModels(factoryProducer = viewModelFactory {
        val repo = CityRepositoryImpl(AppDatabase.getInstance(requireContext().applicationContext))
        val loadCitiesUseCase = LoadCitiesUseCase(repo)
        val saveCityUseCase = SaveCityUseCase(repo)
        MainViewModel(loadCitiesUseCase, saveCityUseCase)
    })

    private val binding: FragmentMasterDetailsBinding by lazy {
        FragmentMasterDetailsBinding.inflate(layoutInflater)
    }

    private val citiesAdapter = CitiesAdapter(this@MasterDetailsFragment)

    private val newCityObserver = Observer<City> { city ->
        city?.let {
            val insertedPos = citiesAdapter.addCity(city)

            if (isAdditionalInfoEnabled && insertedPos > -1) {
                showToast("${city.name} [${city.color.name}] has been added at pos: $insertedPos")
            }
        }
    }

    private var lastSelectedCity: City? = null

    private var isAdditionalInfoEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvCities.apply {
            adapter = citiesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        citiesAdapter.setCities(viewModel.cities.value ?: emptyList())
        if (isOrientationLand()) {
            binding.showNotSelectedCityMessage = true
        }

        isAdditionalInfoEnabled = isAdditionalInfoOptionEnabled()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.about_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.about -> {
                findNavController().navigate(R.id.showAboutFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.startCityGenerator()
        viewModel.currentCity.observe(viewLifecycleOwner, newCityObserver)
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopCityGenerator()
        viewModel.currentCity.removeObserver(newCityObserver)
        lastSelectedCity = null
    }

    override fun isToolbarShown() = true
    override fun getTitle() = lastSelectedCity?.name ?: "Generated cities"
    override fun getToolbarColor() = lastSelectedCity?.color?.value ?: getDefaultColor()

    override fun onCityItemClicked(city: City) {
        lastSelectedCity = city

        if (isOrientationPort()) {
            val action = MasterDetailsFragmentDirections.openDetailsFragment(city)
            findNavController().navigate(action)
        } else {
            prepareMap()
            updateToolbarView()
        }
    }

    override fun onMapReady(mapView: GoogleMap) {
        lastSelectedCity?.let { city ->
            binding.showNotSelectedCityMessage = false

            val latLng = CityLocationAdapter.toLatLng(city.location)
            CameraUpdateFactory.newLatLngZoom(latLng, 13f).let {
                val marker = MarkerOptions()
                    .position(latLng)
                    .title(city.name)

                mapView.addMarker(marker)
                mapView.moveCamera(it)
            }
        }
    }

    private fun prepareMap() {
        (childFragmentManager.findFragmentById(R.id.mapView) as? SupportMapFragment)?.apply {
            getMapAsync(this@MasterDetailsFragment)
        }
    }
}