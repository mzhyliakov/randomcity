package com.example.randomcity.app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.randomcity.R
import com.example.randomcity.data.adapters.CityLocationAdapter
import com.example.randomcity.databinding.FragmentDetailsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions

class DetailsFragment : BaseFragment(), OnMapReadyCallback {

    private val binding: FragmentDetailsBinding by lazy {
        FragmentDetailsBinding.inflate(layoutInflater)
    }

    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        (childFragmentManager.findFragmentById(R.id.mapView) as? SupportMapFragment)?.apply {
            getMapAsync(this@DetailsFragment)
        }
    }

    override fun onMapReady(mapView: GoogleMap) {
        val latLng = CityLocationAdapter.toLatLng(args.city.location)
        CameraUpdateFactory.newLatLngZoom(latLng, 13f).let {
            val marker = MarkerOptions()
                .position(latLng)
                .title(args.city.name)

            mapView.addMarker(marker)
            mapView.moveCamera(it)
        }
    }

    override fun isToolbarShown() = true
    override fun getTitle() = args.city.name
    override fun getToolbarColor() = args.city.color.value
}