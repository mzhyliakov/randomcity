package com.example.randomcity.app

import com.example.randomcity.R

object Constants {

    const val SETTINGS_FILE = "settings"
    const val ADDITIONAL_KEY_RES_ID = R.string.additional_info_key
}