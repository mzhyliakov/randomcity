package com.example.randomcity.app.fragments

import android.content.Context
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.randomcity.R
import com.example.randomcity.app.Constants
import com.example.randomcity.app.MainActivity
import com.example.randomcity.app.Utils

open class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateToolbarView()
    }

    fun switchTo(destinationResId: Int) {
        findNavController().popBackStack()
        findNavController().navigate(destinationResId)
    }

    fun isOrientationPort() =
        requireActivity().resources.configuration.orientation == ORIENTATION_PORTRAIT

    fun isOrientationLand() = !isOrientationPort()

    fun getDefaultColor() = ContextCompat.getColor(requireContext(), R.color.colorPrimary)

    protected fun updateToolbarView() {
        val isToolbarVisible = isToolbarShown()

        (requireActivity() as? MainActivity)?.let {
            if (isSysBarShown()) {
                it.showSystemUI()
            } else {
                it.hideSystemUI()
            }

            if (isToolbarVisible) {
                it.setTitle(getTitle())
            }
            it.setToolbarColors(getToolbarColor(), getToolbarTextColor())
            it.showToolbar(isToolbarVisible)
        }
    }

    protected fun isAdditionalInfoOptionEnabled(): Boolean {
        requireContext().getSharedPreferences(Constants.SETTINGS_FILE, Context.MODE_PRIVATE)
            ?.let { sharedPrefs ->
                return sharedPrefs.getBoolean(getString(Constants.ADDITIONAL_KEY_RES_ID), false)
            }
        return false
    }

    protected fun showToast(message: String, timeLength: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(requireContext(), message, timeLength).show()
    }

    open fun isSysBarShown(): Boolean = true
    open fun isToolbarShown(): Boolean = false
    open fun getTitle(): String = ""
    open fun getToolbarColor(): Int = getDefaultColor()
    open fun getToolbarTextColor() = Utils.getContrastColor(getToolbarColor())
}