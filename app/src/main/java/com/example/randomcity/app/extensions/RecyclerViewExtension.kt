package com.example.randomcity.app.extensions

import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcity.R

fun RecyclerView.createDefaultDivider(): RecyclerView.ItemDecoration {
    val divider = DividerItemDecoration(context, LinearLayout.VERTICAL)
    ContextCompat.getDrawable(context, R.drawable.list_divider)?.let {drawable ->
        divider.setDrawable(drawable)
    }
    return divider
}