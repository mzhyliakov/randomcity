package com.example.randomcity.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CityColor(val name: String, val value: Int): Parcelable