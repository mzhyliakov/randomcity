package com.example.randomcity.domain.repositories

import com.example.randomcity.domain.entities.City
import io.reactivex.Completable
import io.reactivex.Single

interface CityRepository {
    fun loadAll() : Single<List<City>>
    fun save(city: City): Completable
}