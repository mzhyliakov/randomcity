package com.example.randomcity.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class City(
    val name: String,
    val location: CityLocation,
    val color: CityColor,
    val createdAt: Long = System.currentTimeMillis()
): Parcelable