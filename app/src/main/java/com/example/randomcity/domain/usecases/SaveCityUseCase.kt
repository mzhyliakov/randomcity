package com.example.randomcity.domain.usecases

import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.repositories.CityRepository
import io.reactivex.Completable

class SaveCityUseCase(private val repository: CityRepository): CompletableUseCase<City>() {
    override fun buildUseCaseCompletable(params: City): Completable {
        return repository.save(params)
    }
}