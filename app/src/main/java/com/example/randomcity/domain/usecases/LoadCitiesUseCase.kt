package com.example.randomcity.domain.usecases

import com.example.randomcity.domain.entities.City
import com.example.randomcity.domain.repositories.CityRepository
import io.reactivex.Single

class LoadCitiesUseCase(private val repository: CityRepository) :
    SingleUseCase<List<City>, Nothing?>() {
    override fun buildUseCaseSingle(params: Nothing?): Single<List<City>> {
        return repository.loadAll()
    }
}