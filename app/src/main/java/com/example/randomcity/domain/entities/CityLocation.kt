package com.example.randomcity.domain.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CityLocation(val lat: Double, val lon: Double): Parcelable